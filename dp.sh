echo Downloading
apt update -y && apt upgrade -y 
apt install xvkbd nano icewm lxterminal -y
DEBIAN_FRONTEND=noninteractive apt install xrdp -y
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip 
unzip ngrok-stable-linux-amd64.zip
sed -i "2i unset DBUS_SESSION_BUS_ADDRESS" /etc/xrdp/startwm.sh
sed -i "3i unset XDG_RUNTIME_DIR" /etc/xrdp/startwm.sh
sed -i "4i .$HOME/.profile" /etc/xrdp/startwm.sh
sudo sed -i.bak '/fi/a icewm-session \n' /etc/xrdp/startwm.sh
sudo service xrdp start
clear
